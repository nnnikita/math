package test.com.math;

import com.math.services.Services;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ServicesTest {

    Services cut = new Services();

    static Arguments[] getDistanceDegTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1900,45,28310.805300713557),
                Arguments.arguments(1500,44,17630.028436352357),
        };
    }

    @ParameterizedTest
    @MethodSource("getDistanceDegTestArgs")
    void getDistanceDegTest(int speed, int angle, double expected){
        Assertions.assertEquals(expected, cut.getDistanceDeg(speed, angle));
    }


    static Arguments[] getDistanceRadTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1900,3,-7910.477767511669),
                Arguments.arguments(1500,4,17453.04594573455),
        };
    }

    @ParameterizedTest
    @MethodSource("getDistanceRadTestArgs")
    void getDistanceRadTest(int speed, double angle, double expected){
        Assertions.assertEquals(expected, cut.getDistanceRad(speed, angle));
    }


    static Arguments[] getDistanceBetweenCarsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(7,10,5,1,22),
                Arguments.arguments(8,10,7,4,79),
        };
    }

    @ParameterizedTest
    @MethodSource("getDistanceBetweenCarsTestArgs")
    void getDistanceBetweenCarsTest(int speedFirstCar,int speedSecondCar,int distanceBetweenCars, int time,int expected){
        Assertions.assertEquals(expected, cut.getDistanceBetweenCars(speedFirstCar,speedSecondCar,distanceBetweenCars,time));
    }


    static Arguments[] getDotInsideTestArgs(){
        return new Arguments[]{
                Arguments.arguments(0, -0.5, true),
                Arguments.arguments(2, 4, false),
        };
    }

    @ParameterizedTest
    @MethodSource("getDotInsideTestArgs")
    void getDotInsideTest(double x, double y, boolean expected){
        Assertions.assertEquals(expected, cut.getDotInside(x, y));
    }


    static Arguments[] getCountExpressionTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4,4.133316366878097),
                Arguments.arguments(5,3.8499021093955546),
        };
    }

    @ParameterizedTest
    @MethodSource("getCountExpressionTestArgs")
    void getCountExpressionTest(double x, double expected){
        Assertions.assertEquals(expected, cut.getCountExpression(x));
    }
}
