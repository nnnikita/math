package com.math.services;

public class Services {

    //1.Стрельба из гаубицы. Дан угол возвышения ствола а и начальная скорость полёта снаряда. v км/ч.
    // Вычислить расстояние полёта снаряда. Реализовать решения для угла в градусах и в радианах.

    //Градусы
    public double getDistanceDeg(int speed, int angle) {
        double distance;
        double g = 9.81;
        double angleRad = Math.toRadians(angle);
        double speedMS = speed * 1000 / 3600;
        distance = ((speedMS * speedMS) * (2 * Math.sin(angleRad) * Math.cos(angleRad))) / g;
        return distance;
    }
    //Радианы
    public double getDistanceRad(int speed, double angle) {
        double distance;
        double g = 9.81;
        double speedMS = speed * 1000 / 3600;
        distance = ((speedMS * speedMS) * (2 * Math.sin(angle) * Math.cos(angle))) / g;
        return distance;
    }

    //2.Скорость первого автомобиля v1 км/ч, второго — v2 км/ч, расстояние между ними s км.
    //Какое расстояние будет между ними через t ч, если автомобили 	движутся в разные стороны?
    public int getDistanceBetweenCars (int speedFirstCar, int speedSecondCar, int initialDistance, int time) {
        int finalDistance = initialDistance + ((speedFirstCar + speedSecondCar) * time);
        return finalDistance;
    }

    //3.Записать логическое выражение, принимающее значение 1, если точка лежит внутри заштрихованной области, иначе — 0.

    public boolean getDotInside (double x, double y) {
        int x1 = -2;
        int y1 = 2;
        int x2 = 0;
        int y2 = 0;
        int x3 = 0;
        int y3 = -1;
        int x4 = 2;
        int y4 = 2;

        double firstEdgeLeft = (x1 - x) * (y2 - y1) - (x2 - x1) * (y1 - y);
        double secondEdgeLeft = (x2 - x) * (y3 - y2) - (x3 - x2) * (y2 - y);
        double thirdEdgeLeft = (x3 - x) * (y1 - y3) - (x1 - x3) * (y3 - y);
        double firstEdgeRight = (x4 - x) * (y2 - y4) - (x2 - x4) * (y4 - y);
        double secondEdgeRight = (x2 - x) * (y3 - y2) - (x3 - x2) * (y4 - y);
        double thirdEdgeRight = (x3 - x) * (y4 - y3) - (x4 - x3) * (y3 - y);


        if (firstEdgeLeft >= 0 && secondEdgeLeft >= 0 && thirdEdgeLeft >= 0) {
            return true;
        } else if (firstEdgeLeft <= 0 && secondEdgeLeft <= 0 && thirdEdgeLeft <= 0) {
            return true;
        } else if (firstEdgeRight >= 0 && secondEdgeRight >= 0 && thirdEdgeRight >= 0) {
            return true;
        } else if (firstEdgeRight <= 0 && secondEdgeRight <= 0 && thirdEdgeRight <= 0) {
            return true;
        } else {
            return false;
        }
    }

    //4.Вычислить значение выражения.

    public double getCountExpression(double x) {
        double z = 6 * Math.log(Math.sqrt(Math.exp(x + 1) + (2 * Math.exp(x) * Math.cos(x)))) /
                Math.log(x - Math.exp(x + 1) * Math.sin(x)) + Math.abs(Math.cos(x) / Math.exp(Math.sin(x)));
        return z;
    }
}
