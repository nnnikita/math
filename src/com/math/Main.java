package com.math;

import com.math.services.Services;

public class Main {

    public static void main(String[] args) {

        Services services = new Services();
        //1.
        System.out.println(services.getDistanceDeg(1900, 45));
        System.out.println(services.getDistanceRad(1900, 3));
        //2.
        System.out.println(services.getDistanceBetweenCars(5,6,7, 2));
        //3.
        System.out.println(services.getDotInside(0,-0.5));
        //4.
        System.out.println(services.getCountExpression(4));

    }
}
